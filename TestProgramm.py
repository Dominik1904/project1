import os
import re
import sys
from configparser import ConfigParser
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-z1", "--zahl1", dest="Zahl1", help="Zahl1 für die Rechenopeeration", type=int)
parser.add_argument("-o", "--op", dest="Operator", help="Operator für die Rechenopeeration")
parser.add_argument("-z2", "--zahl2", dest="Zahl2", help="Zahl2 für die Rechenopeeration", type=int)
args = parser.parse_args()

Config = ConfigParser()
Config.read("Config.ini", encoding='utf-8')

inhaltConfig= Config.get('variables','VAR_FOR_SCRIPT')

Erg = ""

if args.Operator == "+":
   Erg = args.Zahl1 + args.Zahl2
elif args.Operator=="-":
   Erg = args.Zahl1 - args.Zahl2
elif args.Operator=="*":
   Erg = args.Zahl1 * args.Zahl2
elif args.Operator=="/" and args.Zahl2 != 0:
   Erg = args.Zahl1 / args.Zahl2
if Erg !="":
   print (int(Erg));
else:
   print ("Fehler in der Eingabe")
