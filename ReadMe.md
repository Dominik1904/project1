﻿# Programm #

## Taschenrechner ##


Code für das Auswerten der Rechnung
```python
if args.Operator == "+":
		Erg = args.Zahl1 + args.Zahl2
elif args.Operator=="-":
		Erg = args.Zahl1 - args.Zahl2
elif args.Operator=="*":
		Erg = args.Zahl1 * args.Zahl2
elif args.Operator=="/" and args.Zahl2 != 0:
		Erg = args.Zahl1 / args.Zahl2
```

[mein Repository]: https://bitbucket.org/Dominik1904/project-1/src/master/
*Besuchen Sie [mein Repository][] für weitere Informationen.*

\*Von Sternchen umgeben\*

`Inline-Quelltext` 

![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/atlassian/adf-builder-javascript.svg)

# ToDo: #
* Weitere Fehler beheben
* Code vereinfachen
* evtl. Funktionen erstellen
* Variablen deklaration beschreiben
 